<div class="form-group">
	{!! Form::label('name', 'Nombre del Usuario') !!}
	{!! Form::text('name', null, ['class'=>'form-control']) !!}
</div>
<p>{{ $user->email }}</p>
<hr>
<h3>Lista de roles</h3>
<div class="form-group">	
	<ul class="list-unstyled">
		@foreach($roles as $role)
			<li>
				{{ Form::checkbox('roles[]', $role->id, null) }}
				{{ $role->name }}
				<em>({{ $role->description ?: 'Sin descripción'}})</em>
			</li>
		@endforeach
	</ul>
</div>
<div class="form-group">
	{!! Form::submit('Guardar', ['class'=>'btn btn-sm btn-primary']) !!}
</div>