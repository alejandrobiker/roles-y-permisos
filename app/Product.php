<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // Metodo de asignación masiva
    protected $fillable = [
        'name', 'description',
    ];

}
