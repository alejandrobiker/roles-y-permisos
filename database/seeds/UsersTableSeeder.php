<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role; //Importamos el modelos role


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 20)->create();
    	
        //rol por defecto para utilizar el sistema con todos los permisos
        Role::create([
        	'name'		=> 'Admin',
        	'slug'		=> 'Admin',
        	'special'	=> 'all-access'
        ]);

    }
}
