<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Routes

/*--- Verifica si esta autenticado ---*/
Route::middleware(['auth'])->group(function(){

	//Roles
	@include('permisos/permission_role.php');

	//Products
	@include('permisos/permission_products.php');
	
	//Users
	@include('permisos/permission_users.php');
	
});


